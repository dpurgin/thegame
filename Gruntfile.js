module.exports = function(grunt) { 
    let config = {
        browserify: {       
            dist: {     
                options: {
                    transform: [
                        ["babelify", { 
                            presets: "env"
                        }]
                    ]
                },

                files: {
                    "server/public/js/app.js": "client/js/app.js"
                }
            }
        },

        less: { 
            "server/public/style/style.css": "client/style/style.less"
        },

        copy: { 
            dist: {
                files: [{
                    expand: true,
                    src: [ "index.html", "lib/**" ],
                    cwd: "client",                    
                    dest: "server/public/"
                }, {
                    expand: true, 
                    src: [ "style/*.ttf", "style/*.css" ],
                    cwd: "client",
                    dest: "server/public/"
                }, {
                    expand: true, 
                    src: [ "sounds/**" ],
                    cwd: "client",
                    dest: "server/public/"
                }]
            }
        },

        watch: {
            js: {
                files: "client/js/**",
                tasks: [ "browserify" ]
            },

            css: { 
                files: "client/style/*.less", 
                tasks: [ "less" ]
            },

            static: {
                files: [ "client/index.html",  "client/data/**" ],
                tasks: [ "copy" ]
            }
        },

        clean: { 
            files: [ "server/public/**" ]
        }
    };

    grunt.initConfig(config);

    grunt.loadNpmTasks("grunt-browserify");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-clean");

    grunt.registerTask("default", [
        "browserify",
        "less", 
        "copy"
    ]);
};