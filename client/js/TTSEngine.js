/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Observable from "./Observable";

// Wrapper class for ResponsiveVoice.JS
// https://responsivevoice.org;
export default class TTSEngine extends Observable {
    constructor() { 
        super();
        
        // queue of messages to speak
        // if there's a new message incoming to the engine 
        // and the previous one is still played back, normally ResponsiveVoice 
        // stops the currently played back one and starts the new one. This is 
        // undesired in The Game, therefore queue of messages
        this.m_queue = [];        

        // responsiveVoice events don't seem to work, therefore own events
        this.m_queueCounter = 1;  // unique id of a message
        this.m_playingId = null;  // currently playing id

        // Map of available voices per country
        this.m_voicesMap = new Map();

        let voices = responsiveVoice.getVoices(); 

        let mapping = {
            "uk": [ "UK English", "UK", "Scottish", "Irish" ],
            "us": [ "US English", "US" ],
            "gr": [ ], 
            "tr": [ "Turkish" ],
            "ru": [ "Russian", "Русский" ],
            "it": [ "Italian", "Italiano" ],
            "fr": [ "French", "Français" ],
            "de": [ "Deutsch", "German" ],
            "es": [ "Spanish", "Español" ],
            "nl": [ "Dutch" ]
        };

        // remap available voices
        for (const voice of voices) {
            for (const country in mapping) {
                for (const entry of mapping[country]) { 
                    if (voice.name.indexOf(entry) != -1) {
                        this._appendToMap(country, voice.name);
                        break;
                    }
                }
            }
        }

        setInterval(() => this._processQueue(), 500);
    }

    altitudeForTts(altitude) { 
        var text = "";

        if (altitude < 10000) {
            text = String(altitude) + " feet";
        } else {
            text = "flight level " + this.numberForTts(altitude / 100, 3);
        }

        return text;
    }

    callsignForTts(aircraft) { 
        return aircraft.flight.airline.tts + " " + this.numberForTts(aircraft.flight.flightNumber, 3);
    }

    numberForTts(number, padZeros) { 
        let str = String(number).padLeft(padZeros, "0");
        let result = "";

        for (let i = 0; i < str.length; ++i) {
            if (str[i] == "0") 
                result += "zero";
            else if (str[i] == "1") 
                result += "one"; 
            else if (str[i] == "2") 
                result += "two"; 
            else if (str[i] == "3") 
                result += "tree"; 
            else if (str[i] == "4") 
                result += "four"; 
            else if (str[i] == "5") 
                result += "five"; 
            else if (str[i] == "6") 
                result += "six"; 
            else if (str[i] == "7") 
                result += "seven"; 
            else if (str[i] == "8") 
                result += "eight";
            else if (str[i] == "9")
                result += "niner"; 
            else 
                result += str[i]; 
            
            result += " ";
        }

        return result;
    }

    // return random voice for a country. If no such country, try US, 
    // it's most likely there
    pickVoice(country) { 
        if (this.m_voicesMap.has(country)) {
            return this.m_voicesMap.get(country).pickOne();
        } else if (country !== "us") {
            return this.pickVoice("us");
        } else {
            return undefined;
        }
    }

    // put a message to the queue
    // returns unique message id in the queue. this is used later for emitting signals
    // see _processQueue
    speak(text, voice) { 
        this.m_queue.push({
            "text": text,
            "voice": voice,
            "id": ++this.m_queueCounter
        });

        setTimeout(() => this._processQueue(), 0.1);

        return this.m_queueCounter;
    }

    _appendToMap(country, name) { 
        if (!this.m_voicesMap.has(country))
            this.m_voicesMap.set(country, []);
        
        this.m_voicesMap.get(country).push(name);
    }

    // process queue of messages. If no message is being played back, 
    // speak the next one. 
    // Two signals are emitted: 
    //  * playbackStarted(itemId) 
    //  * playbackEnded(itemId)
    _processQueue() { 
        if (responsiveVoice.isPlaying()) {
            console.log("TTS busy... waiting");
        } else if (this.m_playingId) {
            this.emit("playbackEnded", this.m_playingId);
            this.m_playingId = null;
        }
            
        if (!responsiveVoice.isPlaying() && this.m_queue.length > 0) {

            let item = this.m_queue[0];
            this.m_queue.splice(0, 1);

            responsiveVoice.speak(item.text, item.voice, {
                onstart: () => { 
                    console.log(`voice: ${item.voice}; text: ${item.text}, id: ${item.id}`);
                    this.m_playingId = item.id;
                    this.emit("playbackStarted", this.m_playingId);
                }
            });

        }
    }
}