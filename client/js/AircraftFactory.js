/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Airline from "./Airline";
import Flight from "./Flight";
import { Landmark } from "./Landmark";
import {
    Longitude, 
    Latitude
} from "./Coordinate";
import { 
    B737Aircraft,
    B747Aircraft,
    A320Aircraft,
    A330Aircraft 
} from "./Aircraft";

export default class AircraftFactory {
    constructor() {
        this.m_knownAirlines = [
            new Airline("DLH", "Lufthansa", "Lufthansa", "de"),
            new Airline("AUA", "Austrian Airlines", "Austrian", "de"),
            new Airline("NLY", "Niki", "Niki", "de"),
            new Airline("BER", "Air Berlin", "Air Berlin", "de"),
            new Airline("EWG", "Eurowings", "Eurowings", "de"),
            new Airline("GEC", "Lufthansa Cargo", "Lufthansa Cargo", "de"),
            new Airline("AFL", "Aeroflot", "Aeroflot", "ru"),
            new Airline("GLP", "Globus Airlines", "Globus", "ru"),
            new Airline("AAL", "American Airlines", "American", "us"),
            new Airline("DAL", "Delta Airlines", "Delta", "us"),
            new Airline("UAL", "United Airlines", "United", "us"),
            new Airline("TOM", "Thomson Airlines", "Tomson", "us"), 
            new Airline("BAW", "British Airways", "Speedbird", "uk"),
            new Airline("EZY", "EasyJet", "Easy", "uk"),
            new Airline("KLM", "KLM", "K L M", "nl"),
            new Airline("KLC", "KLM City Hopper", "City", "nl"),
            new Airline("AEE", "Aegean Airlines", "Aegean", "gr"),
            new Airline("AFR", "Air France", "Airfrans", "fr"),
            new Airline("AZA", "Alitalia", "Alitalia", "it"),
            new Airline("AAF", "Aigle Azur", "Aigle Azur", "fr")
        ];

        this.m_callsignMap = new Map();

        for (const airline of this.m_knownAirlines) {
            this.m_callsignMap.set(airline.icao, airline);
        }

        this.m_knownAircrafts = [
            B737Aircraft.create, 
            B747Aircraft.create,
            A320Aircraft.create, 
            A330Aircraft.create
        ];
    }

    generate(atcSector) {
        // decide whether generate departing or arriving acft        
        let isArriving = (Math.random() < 0.5);
        
        let lon = null; 
        let lat = null; 
        let initialPos = null;
        let altitude = 0; 
        let heading = 0;
        let ias = 0;

        let route = [];
        let destinationAltitude = 0;

        // generate airport 
        let apt = atcSector.airports.pickOne();

        let departingThreshold = null;
        let star = null;
        let sid = null;
        
        // if arrival: generate STAR and altitude 
        if (isArriving) {
            star = atcSector.stars.pickOne();
            altitude = star.altitudes.pickOne();

            // generate initial position from the first fix on STAR and add some random 1nm noise 
            // 1nm is 1/60 = 0.017 of degree 
            lon = new Longitude(star.route[0].longitude.toNumeric() +
                                Math.random() * 0.017 * (Math.random() > 0.5? 1: -1));

            lat = new Latitude(star.route[0].latitude.toNumeric() + 
                               Math.random() * 0.017 * (Math.random() > 0.05? 1: -1));

            initialPos = new Landmark("", lat, lon); 
            heading = initialPos.headingTo(star.route[1]);

            ias = (altitude > 10000? Number(240).noise(0.1): Number(220).noise(0.1));

            // generate route for this flight
            for (const fix of star.route) { 
                route.push(fix);
            }

            // compute length of the glideslope
            // assume glideslope entry is at 3000 ft above threshold
            // then convert this value to nautical miles then to degrees 
            // 1nm = 6076.12 ft
            // 1 deg = 60 nm
            let landingThreshold = apt.landingThreshold;
            let glideslope = 3000 / (landingThreshold.glideslopeGradient / 100) / 6076.12 / 60;

            // compute glideslope entry position as final point of the route 
            route.push(new Landmark("Glideslope", 
                new Latitude(landingThreshold.latitude.toNumeric() + 
                             glideslope * Math.cos(Number(landingThreshold.heading + 180).toRad())),
                new Longitude(landingThreshold.longitude.toNumeric() + 
                              glideslope * Math.sin(Number(landingThreshold.heading + 180).toRad())))
            );

            destinationAltitude = 3000;
        } else {  // departing aircraft
            departingThreshold = apt.departingThreshold;

            let rwy = departingThreshold.runway;

            // generate spawn point
            if (rwy.thresholds[0] === departingThreshold) {
                heading = rwy.thresholds[0].heading;

                lon = rwy.thresholds[1].longitude;
                lat = rwy.thresholds[1].latitude;
                altitude = rwy.thresholds[1].altitude + Number(600).noise(0.1);
            } else {
                heading = rwy.thresholds[1].heading;

                lon = rwy.thresholds[0].longitude; 
                lat = rwy.thresholds[0].latitude;
                altitude = rwy.thresholds[0].altitude + Number(600).noise(0.1);
            }

            // choose initial air speed
            ias = Number(210).noise(0.03);

            // create landmark for initial pos (compute heading afterwards)
            initialPos = new Landmark("", lat, lon);

            // choose SID and set initial climbs/turns 
            sid = atcSector.getSidsByRunway(departingThreshold.name).pickOne();

            for (const fix of sid.route) {
                route.push(fix);
            }

            let destination = sid.destinations.pickOne();

            if (destination !== route.back())
                route.push(destination);

            destinationAltitude = sid.destinationAltitudes.pickOne();
        }

        // generate airline and flight number 
        let flightNumber = String(Math.round(Math.random() * 998) + 1); 
        let flight = new Flight(this.m_knownAirlines.pickOne(), 
                                flightNumber, 
                                route, 
                                destinationAltitude, 
                                sid, 
                                star);

        let aircraft = this.m_knownAircrafts.pickOne()(
            flight, 
            lat, lon, 
            altitude, 
            heading, 
            ias,
            g_app.ttsEngine.pickVoice(flight.airline.country));

        if (!isArriving) {

            aircraft.changeAltitude(sid.initialClimb);

            if (sid.initialTurn == "left")
                aircraft.turnLeft(initialPos.headingTo(sid.route[0]));
            else if (sid.initialTurn == "right") 
                aircraft.turnRight(initialPos.headingTo(sid.route[0]));
        }

        return aircraft;
    }
}