/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Observable from "./Observable";
import AircraftFactory from "./AircraftFactory";
import AtcSector from "./AtcSector";
import CanvasRenderer from "./CanvasRenderer";
import TTSEngine from "./TTSEngine";
import RadioStack from "./RadioStack";

import OpenSectorDialogController from "./uicontrollers/OpenSectorDialogController";
import NavbarController from "./uicontrollers/NavbarController";
import CommandsMenuController from "./uicontrollers/CommandsMenuController";
import OptionsController from "./uicontrollers/OptionsController";
import MainOptionsModalController from "./uicontrollers/MainOptionsModalController";
import ScoreboardModalController from "./uicontrollers/ScoreboardModalController";

export default class Application extends Observable {
    constructor() {
        super();

        this.m_aircrafts = [];
        this.m_aircraftsMap = new Map();

        this.m_conflictsDetected = false;

        this.m_aircraftFactory = new AircraftFactory();        

        this.m_timerId = null;
        this.m_lastGameCycle = null;
        this.m_atcSector = null;
        this.m_renderer = new CanvasRenderer(this);

        this.m_stats = new Map();
        this.m_stats.set("elapsed", 0);
        this.m_stats.set("conflict", 0);
        this.m_stats.set("aircrafts", 0);
        this.m_stats.set("served", 0);
        this.m_stats.set("lastSaved", null);

        this.m_difficultyLevel = null;
        this.m_difficultyFactor = 1.0; 
        this.m_maxAircrafts = Math.round(15 / this.m_difficultyFactor);
        this.m_destinationRadius = 2.5; // nm
        this.m_speedUp = null;
        this.m_playerName = null;

        this.m_ttsEngine = new TTSEngine();
        this.m_radioStack = new RadioStack(this, this.m_ttsEngine);

        this.m_theme = null;

        this.m_uiControllers = [
            new OpenSectorDialogController(this),
            new NavbarController(this),
            new CommandsMenuController(this),
            new OptionsController(this),
            new MainOptionsModalController(this),
            new ScoreboardModalController(this)
        ];
        
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            let audio = $("#button_click")[0];
            $("button").mousedown(function(){
                audio.play();
            });
        });

        this.connect("conflictsDetected", (hasConflicts) => {
            if (hasConflicts) {
                $("#conflictSound")[0].play();
            } else {
                $("#conflictSound")[0].pause();
                $("#conflictSound")[0].currentPosition = 0;
            }
        });
    }   

    get aircrafts() { return this.m_aircrafts; }
    get atcSector() { return this.m_atcSector; }
    get conflictsDetected() { return this.m_conflictsDetected; } 
    get destinationRadius() { return this.m_destinationRadius; }
    get difficultyLevel() { return this.m_difficultyLevel; }
    get isRunning() { return !!this.m_timerId; }
    get renderer() { return this.m_renderer; }
    get theme() { return this.m_theme; } 
    get ttsEngine() { return this.m_ttsEngine; } 
    get radioStack() { return this.m_radioStack; } 
    get speedUp() { return this.m_speedUp; }
    get playerName() { return this.m_playerName; }

    set difficultyLevel(val) { 
        console.assert(val === "easy" || val === "normal" || val === "hard" || val === "impossible",
                       `unsupported difficulty level: ${val}`);

        if (this.m_difficultyLevel !== val) {
            if (val === "easy") {
                this.m_difficultyFactor = 1.5;
                this.m_destinationRadius = 7.5;
            } else if (val === "normal") {
                this.m_difficultyFactor = 1.0;
                this.m_destinationRadius = 5;
            } else if (val === "hard") {
                this.m_difficultyFactor = 0.75;
                this.m_destinationRadius = 2.5;
            } else if (val === "impossible") {
                this.m_difficultyFactor = 0.5;
                this.m_destinationRadius = 1;
            }

            this.m_difficultyLevel = val;
            this.m_maxAircrafts = Math.round(15 / this.m_difficultyFactor);

            this.emit("difficultyLevelChanged", this.difficultyLevel);
        }
    }

    set playerName(val) { 
        if (this.m_playerName !== val) {
            this.m_playerName = val;

            localStorage.setItem("playerSettings", JSON.stringify({
                "name": this.playerName
            }));

            this.emit("playerNameChanged", this.playerName);
        }
    }

    set speedUp(val) { 
        if (this.m_speedUp !== val) { 
            this.m_speedUp = val; 

            this.emit("speedUpChanged", this.speedUp);
        }
    }

    set theme(val) { 
        if (val !== this.m_theme) {
            if (val === "dark") {
                this.m_theme = "dark";
                $("head").append("<link rel=\"stylesheet\" href=\"lib/bootstrap-3.3.7/css/darkly.min.css\">");            
            } else {
                this.m_theme = "light";
                $("head").append("<link rel=\"stylesheet\" href=\"lib/bootstrap-3.3.7/css/flatly.min.css\">");
            }

            this.emit("themeChanged", this.m_theme);            
        }
    }

    getAircraft(callsign){ 
        console.assert(this.m_aircraftsMap.get(callsign), `no such aircraft: ${callsign}`);

        return this.m_aircraftsMap.get(callsign);
    }

    getAirlineCallsign(icao) {
        let airline = this.m_aircraftFactory.m_callsignMap.get(icao);

        return (airline? airline.tts: icao);
    }

    loadSector(sectorData) {
        this.stop();

        if (AtcSector.isValid(sectorData)) {
            this.m_atcSector = new AtcSector(sectorData);

            this.emit("sectorLoaded", true);
        } else {
            this.emit("sectorLoaded", false);
        }    
    }

    // Stops game cycle
    stop() {         
        if (this.isRunning)
        {
            clearInterval(this.m_timerId);

            this.emit("isRunningChanged", false);

            console.log("Simulation stopped");   

            this._sendStats();         
        }
    }

    // Starts game cycle 
    start() {
        if (this.isRunning) 
            this.stop();

        this.m_lastGameCycle = new Date();
        this.m_timerId = setInterval(() => this._gameCycle(), 1000);

        this.emit("isRunningChanged", true);

        console.log("Simulation started");

        $.ajax({
            url: `/api/scores/${this.playerName}/${this.atcSector.title}`, 
            error: (xhr) => {
                if (xhr.status === 404) {
                    $.post(`/api/scores`, {
                        name: this.playerName,
                        sector: this.atcSector.title,
                        elapsed: 0,
                        conflict: 0, 
                        served: 0
                    });
                }
            }
        });

        this.m_stats.set("lastSaved", new Date());
    }

    // Processes single game iteration (moves aircrafts, generates events, etc)
    _gameCycle() {
        let now = new Date();
        let elapsed = (now.valueOf() - this.m_lastGameCycle.valueOf()) * this.m_speedUp;
        
        if (this.m_aircrafts.length < this.m_maxAircrafts && 
            Math.random() < this._newAircraftProbability())  {

            let aircraft = null;
            let hasConflicts = true;
            
            // generate an aircraft so that it doesn't conflict with the existing ones
            // try N times and give up
            let retries = this.m_aircrafts.length * this.m_atcSector.stars.length * 2 + 1;
            for (let i = 0; i < retries  && hasConflicts; ++i) {
                aircraft = this.m_aircraftFactory.generate(this.m_atcSector);
                hasConflicts = false;
                
                for (const existing of this.m_aircrafts) { 
                    if (aircraft.inConflictZone(existing)) {
                        hasConflicts = true; 
                        break;
                    }
                }
            }

            if (!hasConflicts) {
                this.m_aircrafts.push(aircraft);
                this.m_aircraftsMap.set(aircraft.flight.callsign, aircraft);

                this.emit("aircraftAdded", aircraft);
            } else {
                console.warn("Gave up trying to generate an aircraft!");
            }
        }

        for (const acft of this.m_aircrafts)
            acft.processGameCycle(elapsed);

        // now after all position changes have been processed, 
        // check all possible conflicts. This has complexity 
        // O(n^2) maybe there's a better way...
        let currentlyHasConflicts = false;

        for (const acft of this.m_aircrafts)
            acft.hasConflict = false;

        for (let i = 0; i < this.m_aircrafts.length - 1; ++i) {
            let acft1 = this.m_aircrafts[i];

            for (let j = i + 1; j < this.m_aircrafts.length; ++j) {
                let acft2 = this.m_aircrafts[j];

                // skip if both were already marked for conflict 
                if (acft1.hasConflict && acft2.hasConflict)
                    continue;

                // compute distance between two aircrafts in nm 
                var dist = Math.sqrt(Math.pow(acft1.longitude.toNumeric() - acft2.longitude.toNumeric(), 2) + 
                                     Math.pow(acft1.latitude.toNumeric() - acft2.latitude.toNumeric(), 2)) * 60;
                            
                // assume that separation is horizontal separation is 5 nm 
                // vertical separation 1000 ft. 5 nm is 5 longitude minutes.
                // Check against 900 ft vertically due to some irregularity in altitudes

                if (dist < 5 && Math.abs(acft1.altitude - acft2.altitude) < 900) {
                    acft1.hasConflict = true;
                    acft2.hasConflict = true;
                    currentlyHasConflicts = true;
                }                                            
            }                
        }

        if (currentlyHasConflicts != this.m_conflictsDetected) { 
            this.m_conflictsDetected = currentlyHasConflicts;
            this.emit("conflictsDetected", this.m_conflictsDetected);
        }

        // check if aircraft reached destination at desired altitude 
        let removedAircrafts = [];

        for (const acft of this.aircrafts) {
            if (acft.flight.destination.distanceTo(acft.latitude, acft.longitude) < this.m_destinationRadius / 60 && 
                Math.abs(acft.flight.destinationAltitude - acft.altitude < 300)) { 

                removedAircrafts.push(acft);
            }
        }

        for (const acft of removedAircrafts) {
            this.m_aircraftsMap.delete(acft.flight.callsign);

            for (let i = 0; i < this.m_aircrafts.length; ++i) {
                if (this.m_aircrafts[i] == acft) {
                    this.m_aircrafts.splice(i, 1);
                }
            }

            this.m_stats.set("served", this.m_stats.get("served") + 1);
            this.emit("aircraftRemoved", acft);
        }
                
        this.m_stats.set("elapsed", this.m_stats.get("elapsed") + elapsed / 1000);
        this.m_stats.set("aircrafts", this.m_aircrafts.length);

        if (this.m_conflictsDetected) {        
            this.m_stats.set("conflict", this.m_stats.get("conflict") + elapsed / 1000);            
        }

        this.emit("statsUpdated", this.m_stats);

        if (this.m_stats.get("lastSaved") !== null && 
            now.valueOf() - this.m_stats.get("lastSaved").valueOf() > 10000) {
                this._sendStats();
        }

        this.m_lastGameCycle = now;
    }

    _newAircraftProbability() {
        if (this.m_aircrafts.length == 0) 
            return 1;
        else if (this.m_stats.get("elapsed")  < 300) // uniform distribution for the first 5 min
            return 1 / 60 / this.m_difficultyFactor; 
        else // maintain constant density of aircrafts
            return Math.pow(2, -this.m_difficultyFactor * this.m_aircrafts.length);
    }

    _sendStats() {
        $.ajax({
            type: "PUT",
            url: `/api/scores/${this.playerName}/${this.atcSector.title}`,
            data: {
                elapsed: this.m_stats.get("elapsed"),
                conflict: this.m_stats.get("conflict"),
                served: this.m_stats.get("served")   
            }
        });  

        this.m_stats.set("lastSaved", new Date());      
    }
}
