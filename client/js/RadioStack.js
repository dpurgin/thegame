/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Observable from "./Observable";

export default class RadioStack extends Observable {
    constructor(app, ttsEngine) {
        super();

        this.m_app = app;
        this.m_ttsEngine = ttsEngine;

        this.m_atcVoice = null;

        this.m_pendingCommands = new Map();
        this.m_radioTransmitting = false;

        app.connect("aircraftAdded", (...args) => this._onAircraftAdded(...args));

        ttsEngine.connect("playbackStarted", (...args) => this._onTtsPlaybackStarted(...args));
        ttsEngine.connect("playbackEnded", (...args) => this._onTtsPlaybackEnded(...args));

        this.m_staticPlaying = false;
        this.m_staticPaused = true;

        $(document).ready(() => {
            this.m_staticEl = $("#radioStatic")[0];

            this.m_staticEl.onplaying = () => {
                this.m_staticPlaying = true; 
                this.m_staticPaused = false;
            };

            this.m_staticEl.onpause = () => {
                this.m_staticPlaying = false; 
                this.m_staticPaused = true;
            };
        });
    }

    get radioTransmitting() { return this.m_radioTransmitting; }
    get atcVoice() { return (this.m_atcVoice === null? "default": this.m_atcVoice); }
    
    set atcVoice(val) { 
        if (val !== this.m_atcVoice) {
            this.m_atcVoice = (val === "default"? null: val);

            this.emit("atcVoiceChanged", this.atcVoice);
        }
    }

    // initiated by aircraft
    announce(aircraft, announcement) {
        this.m_ttsEngine.speak(announcement, aircraft.voice);
    }

    // issue a command to aircraft. First the command is spoken, then crew's reply
    command(aircraft, command) { 
        let text = this.m_ttsEngine.callsignForTts(aircraft);

        command["callsign"] = aircraft.flight.callsign;

        if (command["turnHeading"]) {
            text += `, turn ${command["turnDirection"]} heading ` + 
                    this.m_ttsEngine.numberForTts(command["turnHeading"], 3);
        }

        if (command["altitude"]) {
            text += ", " + (command["altitude"] > aircraft.altitude? "climb": "descend") + 
                    " and maintain " + this.m_ttsEngine.altitudeForTts(command["altitude"]);
        }

        let commandId = this.m_ttsEngine.speak(text, this.m_atcVoice);
        this.m_pendingCommands.set(commandId, command);
    }

    _onAircraftAdded(aircraft) {
        let text = this.m_app.atcSector.callsign + ", " + 
                   this.m_app.ttsEngine.callsignForTts(aircraft) + " "; 

        if (aircraft.flight.sid) { 
            text += "airborne, " + aircraft.flight.sid.tts + " departure";
        } else {
            text += "with you, " + aircraft.flight.star.tts + " arrival";
        }

        this.announce(aircraft, text);
    }

    _onTtsPlaybackStarted(itemId) { 
        if (!this.m_pendingCommands.has(itemId))
            this._playStatic();

        this.m_radioTransmitting = true;
        this.emit("radioTransmitting", true);
    }
    
    _onTtsPlaybackEnded(itemId) { 
        this._stopStatic();

        this.m_radioTransmitting = false;
        this.emit("radioTransmitting", false);

        if (this.m_pendingCommands.has(itemId)) {
            let command = this.m_pendingCommands.get(itemId);
            let acft = this.m_app.getAircraft(command["callsign"]);

            let response = "";

            if (command["turnDirection"]) {
                if (command["turnDirection"] === "left") {
                    acft.turnLeft(command["turnHeading"]);
                } else if (command["turnDirection"] === "right") {
                    acft.turnRight(command["turnHeading"]);
                }

                response += command["turnDirection"] + " heading " + 
                            this.m_ttsEngine.numberForTts(command["turnHeading"], 3) + ", ";
            }

            if (command["altitude"]) {
                acft.changeAltitude(command["altitude"]);

                response += (command["altitude"] > acft.altitude? "climbing ": "descending ") + 
                            this.m_ttsEngine.altitudeForTts(command["altitude"]);
            }

            response += ", " + this.m_ttsEngine.callsignForTts(acft);

            this.announce(acft, response);

            this.m_pendingCommands.delete(itemId);
        }
    }

    _playStatic() { 
        if (!this.m_staticPlaying) 
            this.m_staticEl.play();
    }

    _stopStatic() { 
        if (this.m_staticPlaying)
            this.m_staticEl.pause();
    }
}
