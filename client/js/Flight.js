/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Airline from "./Airline";

export default class Flight {
    constructor(airline, flightNumber, route, destinationAltitude, sid, star) {
        console.assert(airline instanceof Airline);

        this.m_airline = airline;
        this.m_flightNumber = flightNumber;
        this.m_route = route;
        this.m_destinationAltitude = destinationAltitude;

        this.m_sid = sid;
        this.m_star = star;
    }

    get airline() { return this.m_airline; } 
    get destination() { return this.m_route[this.m_route.length - 1]; }
    get destinationAltitude() { return this.m_destinationAltitude; }
    get flightNumber() { return this.m_flightNumber; }
    get route() { return this.m_route; } 
    get sid() { return this.m_sid; } 
    get star() { return this.m_star; } 

    get callsign() { 
        let flightNumber = String(this.flightNumber);

        while (flightNumber.length < 3)
            flightNumber = "0" + flightNumber;        

        return this.airline.icao + flightNumber;
    }    
}