/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import {
    Latitude, 
    Longitude
} from "./Coordinate";

import {
    Fix, 
    Airport,
    Runway,
    RunwayThreshold
} from "./Landmark";

import {
    STAR, 
    SID
} from "./NavigationProcedure";

export default class AtcSector {
    constructor(sectorData) {
        this.m_fixes = [];
        this.m_fixesMap = new Map();

        this.m_airports = [];

        this.m_stars = [];
        this.m_starsMap = new Map();

        this.m_sids = []; 
        this.m_sidsMap = new Map();
        this.m_sidsByRunway = new Map();

        this.m_title = sectorData["title"];
        this.m_callsign = sectorData["callsign"];

        this.m_centerFix = null;
        this.m_centerZoomLevel = 0;

        for (const fixData of sectorData["fixes"]) {
            let fix = new Fix(fixData["name"], 
                              new Latitude(fixData["latitude"]), 
                              new Longitude(fixData["longitude"]));

            this.m_fixes.push(fix);
            this.m_fixesMap.set(fix.name, fix);
        }

        for (const airportData of sectorData["airports"]) {
            let runways = [];

            for(const runwayData of airportData["runways"]){
                let thresholds = [];

                for(const thresholdData of runwayData["thresholds"]){
                    let threshold = new RunwayThreshold(thresholdData["name"],
                                                        new Latitude(thresholdData["latitude"]),
                                                        new Longitude(thresholdData["longitude"]),
                                                        thresholdData["altitude"],
                                                        thresholdData["glideslopeGradient"],
                                                        thresholdData["heading"]);
                    thresholds.push(threshold);

                    this.m_sidsByRunway.set(threshold.name, []);
                }

                let runway = new Runway(" ", new Latitude(0), new Longitude(0),
                                        runwayData["length"],
                                        runwayData["width"],
                                        runwayData["bearing"],
                                        thresholds);
                runways.push(runway);           
            }

            let apt = new Airport(airportData["name"], 
                                      new Latitude(airportData["latitude"]),
                                      new Longitude(airportData["longitude"]), 
                                      airportData["altitude"], 
                                      runways);
            this.m_airports.push(apt);                
        }

        for (const starData of sectorData["stars"]) { 
            let star = new STAR(starData["name"], 
                                starData["tts"], 
                                this._parseRoute(starData["route"]),
                                starData["altitudes"]);

            this.m_stars.push(star);
            this.m_starsMap.set(star.name, star);
        }

        for (const departureData of sectorData["departures"]) {
            for (const sidData of departureData["sids"]) {
                let destinations = [];
                let destinationAltitudes = [];
                let route = this._parseRoute(sidData["route"]);

                if (sidData["destinations"]) {
                    for (const fixName of sidData["destinations"]) {
                        destinations.push(this.getFix(fixName));
                    }
                }

                for (const altitude of sidData["destinationAltitudes"]) {
                    destinationAltitudes.push(altitude);
                }

                if (destinations.length === 0)
                    destinations.push(route[route.length - 1]);

                let sid = new SID(sidData["name"], 
                                  sidData["tts"], 
                                  route, 
                                  sidData["initialClimb"], 
                                  sidData["initialTurn"], 
                                  destinations, 
                                  destinationAltitudes);

                this.m_sids.push(sid);
                this.m_sidsMap.set(sid.name, sid);                
                this.m_sidsByRunway.get(departureData["runway"]).push(sid);
            }
        }

        this.m_centerFix = this.getFix(sectorData["center"]["fix"]);
        this.m_centerZoomLevel = sectorData["center"]["zoomLevel"];
    }

    static isValid(sectorData) {
        return true;
    }

    getFix(name) { 
        return this._retrieveMapElement(this.m_fixesMap, name);
    }

    getSidsByRunway(rwy) { return this.m_sidsByRunway.get(rwy); } 

    getStar(name) { 
        return this._retrieveMapElement(this.m_starsMap, name);
    }

    get callsign() { return this.m_callsign; } 
    get centerFix() { return this.m_centerFix; } 
    get centerZoomLevel() { return this.m_centerZoomLevel; }
    get fixes() { return this.m_fixes; } 
    get sids() { return this.m_sids; } 
    get stars() { return this.m_stars; } 
    get title() { return this.m_title; }  
    get airports() { return this.m_airports; }  

    _parseRoute(routeData) { 
        let route = [];

        for (const fixName of routeData) {
            route.push(this.getFix(fixName));
        }   

        return route;        
    }

    _retrieveMapElement(map, key) { 
        if (!map.has(key))
            console.warn(`No element in map: ${key}`);

        return map.get(key);
    }
}
