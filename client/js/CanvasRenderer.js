/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import { Longitude, Latitude } from "./Coordinate";

export default class CanvasRenderer {
    constructor(app) {
        this.m_app = app;

        this.m_translationX = 0;
        this.m_translationY = 0;
        this.m_scale = 1;
        this.m_zoomLevel = 0;

        this.m_mouseX = null;
        this.m_mouseY = null;
        this.m_mouseButtons = null;
        this.m_panMode = false;

        this.m_emphasizeHeadings = false;
        this.m_emphasizeDestination = false;

        this.m_highlightedAircraft = null;

        $(document).ready(() => this._initialize());
    }

    get emphasizeDestination() { return this.m_emphasizeDestination; }
    get emphasizeHeadings() { return this.m_emphasizeHeadings; }
    get translationX() { return this.m_translationX; }
    get translationY() { return this.m_translationY; }
    get scale() { return this.m_scale; }

    set emphasizeDestination(val) { this.m_emphasizeDestination = val; } 
    set emphasizeHeadings(val) { this.m_emphasizeHeadings = val; } 

    centerOn(lon, lat, zoomLevel) { 
        console.assert(lon instanceof Longitude, "lon instanceof Longitude"); 
        console.assert(lat instanceof Latitude, "lat instanceof Latitude");
        console.assert(zoomLevel > 0, "zoomLevel > 0");

        this.m_zoomLevel = zoomLevel; 
        this.m_scale = Math.pow(2, this.m_zoomLevel);

        this.m_translationX = -lon.toNumeric() * this.m_scale + this.m_canvas.offsetWidth / 2;
        this.m_translationY = -lat.toNumeric() * this.m_scale - this.m_canvas.offsetHeight / 2;

        this._render();
    }

    lonToX(lon) {
        let val = (lon instanceof Longitude? lon.toNumeric(): Number(lon));

        return this.translationX + val * this.scale;
    }

    latToY(lat) {
        let val = (lat instanceof Latitude? lat.toNumeric(): Number(lat));
        // Y axis of the canvas is in opposite direction of our world coordinates, 
        // thus invert direction
        return -this.translationY - val * this.scale;    
    }    

    nmToPx(nm) {
        return nm / 60 * this.scale;
    }

    xToLon(x) {
        return new Longitude((x - this.translationX) / this.scale);
    }

    yToLat(y) { 
        return new Latitude(-(y + this.translationY) / this.scale);
    }


    _initialize() {
        this.m_canvas = document.getElementById("canvas");        

        this.m_canvas.addEventListener("mousemove", (e) => {
            this._onMouseMove(e);
            this._render();
        });

        this.m_canvas.addEventListener("mousewheel", (e) => {
            this._onMouseWheel(e);
            this._render();
        });

        this.m_canvas.addEventListener("mouseup", (e) => {
            this._onMouseUp(e);
            this._render();
        })

        this.m_canvas.addEventListener("DOMMouseScroll", (e) => {
            this._onMouseWheel(e);
            this._render();
        });

        window.addEventListener("resize", () => this._onResize());
        setInterval(() => this._render(), 40);        

        this._onResize();
    }

    _onMouseUp(event) { 
        console.log("mouse up");

        if (!this.m_panMode) {
            // unselect aircrafts
            for (const acft of this.m_app.aircrafts ) {
                acft.isSelected = false;
            }
            this.m_app.emit("aircraftsDeselected");

            // detect if there is an aircraft or its tag tag under cursor
            for (const acft of this.m_app.aircrafts) { 
                let x = this.lonToX(acft.longitude);
                let y = this.latToY(acft.latitude);

                // check if aircraft square contains the cursor 
                if ((x - 5 <= this.m_mouseX && this.m_mouseX <= x + 5 && 
                     y - 5 <= this.m_mouseY && this.m_mouseY <= y + 5) || 
                // check if aicraft tag contains the cursor
                // if acft is in the I quadrant, its tag is drown in the 2nd quadrant
                   (acft.heading <= 90 && 
                           x + 55 <= this.m_mouseX && this.m_mouseX <= x + 125 && 
                           y + 55 <= this.m_mouseY && this.m_mouseY <= y + 105) || 
                   (x + 55 <= this.m_mouseX && this.m_mouseX <= x + 125 && 
                           y - 75 <= this.m_mouseY && this.m_mouseY <= y - 25))
                {
                    acft.isSelected = true; 
                    this.m_app.emit("aircraftSelected", acft);
                    break;
                }
            }
        }

        this.m_panMode = false;
    }

    _onMouseMove(event) {         
        if (event.buttons == 1) {
            this.m_translationX += (event.offsetX - this.m_mouseX);
            this.m_translationY -= (event.offsetY - this.m_mouseY);

            if (event.offsetX- this.m_mouseY !== 0 || 
                event.offsetY- this.m_mouseY !== 0 ) {
                this.m_panMode = true;
            }
        }

        if (!this.m_panMode) { 
            this.m_highlightedAircraft = null;

            // detect if there is an aircraft under cursor
            for (const acft of this.m_app.aircrafts) { 
                let x = this.lonToX(acft.longitude);
                let y = this.latToY(acft.latitude);

                // check if aircraft square contains the cursor 
                if ((x - 5 <= this.m_mouseX && this.m_mouseX <= x + 5 && 
                     y - 5 <= this.m_mouseY && this.m_mouseY <= y + 5) || 
                // check if aicraft tag contains the cursor
                // if acft is in the I quadrant, its tag is drown in the 2nd quadrant
                   (acft.heading <= 90 && 
                           x + 55 <= this.m_mouseX && this.m_mouseX <= x + 125 && 
                           y + 55 <= this.m_mouseY && this.m_mouseY <= y + 105) || 
                   (x + 55 <= this.m_mouseX && this.m_mouseX <= x + 125 && 
                           y - 75 <= this.m_mouseY && this.m_mouseY <= y - 25))
                {
                    this.m_highlightedAircraft = acft.flight.callsign; 
                    break;
                }
            }            
        }

        this.m_mouseButtons = event.buttons;
        this.m_mouseX = event.offsetX; 
        this.m_mouseY = event.offsetY;
    }

    _onMouseWheel(event) {
        let wheelDelta = 0;

        if (isFinite(event.wheelDeltaY)) {
            // Chrome, Edge provide wheelDeltaY 
            wheelDelta = event.wheelDeltaY;
        } else {
            // Firefox specific: positive event.detail denotes scroll down 
            // See https://www.sitepoint.com/html5-javascript-mouse-wheel/
            wheelDelta = -event.detail;
        }    

        // remember focal point under cursor to translate coordinates after zooming
        let focusLon = this.xToLon(this.m_mouseX); 
        let focusLat = this.yToLat(this.m_mouseY);

        // zoom in/out
        this.m_zoomLevel += (wheelDelta > 0? 1: -1) * 0.1;
        this.m_scale = Math.pow(2, this.m_zoomLevel);

        // compute difference between unzoomed focal point and the zoomed one, 
        // then translate to compensate the drift
        let focusedLonX = this.lonToX(focusLon);
        let focusedLatY = this.latToY(focusLat);

        this.m_translationX -= focusedLonX - this.m_mouseX; 
        this.m_translationY += focusedLatY - this.m_mouseY;
    }

    _onResize() { 
        let el = $(this.m_canvas);

        // resize the canvas manually 
        // Flex layout works for canvas in Chrome but not in Firefox
        el.height($("body").height() - $("#navbar").height())
          .width($("body").width() - $("#commandsMenu").width());

        el.attr("width", el.width())
          .attr("height", el.height());

        this._render();        
    }

    _render() {
        this._showMouseCoordinates();

        let ctx = this.m_canvas.getContext("2d");

        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, this.m_canvas.offsetWidth, this.m_canvas.offsetHeight);

        if (this.m_app.theme === "light") 
            ctx.fillStyle = "#fdf6e3";
        else 
            ctx.fillStyle = "#002b36";

        ctx.fillRect(0, 0, this.m_canvas.offsetWidth, this.m_canvas.offsetHeight);

        if (!this.m_app.atcSector) 
            return; 

        // Fixes
        for (const fix of this.m_app.atcSector.fixes) {
            ctx.font = "1em shareTech";

            ctx.lineJoin = "round";
            ctx.fillStyle = "#657983";
            ctx.strokeStyle = "#657983";
            ctx.lineWidth = 1;

            //Fix Triangles
            ctx.beginPath();            
            ctx.moveTo(this.lonToX(fix.longitude), this.latToY(fix.latitude) - 5);
            ctx.lineTo(this.lonToX(fix.longitude) + 5, this.latToY(fix.latitude) + 5);
            ctx.lineTo(this.lonToX(fix.longitude) - 5, this.latToY(fix.latitude) + 5);
            ctx.closePath();
            ctx.stroke();            
            //Fix Names
            ctx.fillText(fix.name, this.lonToX(fix.longitude) + 5, this.latToY(fix.latitude) - 5);
        }

        for (const aircraft of this.m_app.aircrafts) {
            let callsign = aircraft.flight.callsign;

            // current altitude 
            let textAlt = String(Math.round(aircraft.altitude / 100)).padLeft(3, "0");
            // assigned altitude
            let textAssAlt = String(aircraft.assignedAltitude / 100).padLeft(3, "0");
            // climb/descend indicator
            let verticalState = " ";

            // aircraft geo coordinates to screen
            let aircraftX = this.lonToX(aircraft.longitude);
            let aircraftY = this.latToY(aircraft.latitude);

            if (aircraft.hasConflict){
                ctx.strokeStyle = "#dc322f";
                ctx.fillStyle = "#dc322f";
            } else if (aircraft.isSelected || 
                       this.m_highlightedAircraft === aircraft.flight.callsign) {
                ctx.strokeStyle = "#268bd2";
                ctx.fillStyle = "#268bd2";
            }else{
                ctx.strokeStyle = "#b58900";
                ctx.fillStyle = "#b58900";
            }
            if(aircraft.isClimbing){
                verticalState = "↑";
            }else if(aircraft.isDescending){
                verticalState = "↓";
            }else if(aircraft.isCruising){
                verticalState = " ";
            }
            //Aircraft - Square
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.rect(aircraftX - 2.5, aircraftY - 2.5, 5, 5);
            ctx.stroke();
            //Direction 
            ctx.beginPath();
            ctx.moveTo(aircraftX, aircraftY);
            ctx.lineTo(aircraftX + Math.sin(aircraft.heading * Math.PI / 180) * 50,
                       aircraftY - Math.cos(aircraft.heading * Math.PI / 180) * 50);
            ctx.stroke();                        

            if(aircraft.heading <= 90){
                ctx.beginPath();
                ctx.lineWidth = 1;
                ctx.moveTo(aircraftX + 2.5, aircraftY + 2.5);
                ctx.lineTo(aircraftX + 55, aircraftY + 55);
                ctx.stroke();
                // Aircraft tag
                ctx.fillText(callsign, aircraftX + 55, aircraftY + 70);       
                ctx.fillText(textAlt + verticalState + textAssAlt,
                            aircraftX + 55, aircraftY + 85);         
                ctx.fillText(aircraft.indicatedAirSpeed + " " + aircraft.model,
                            aircraftX + 55, aircraftY + 100);
            }else{
                ctx.beginPath();
                ctx.lineWidth = 1;
                ctx.moveTo(aircraftX + 2.5, aircraftY - 2.5);
                ctx.lineTo(aircraftX + 55, aircraftY - 55);
                ctx.stroke(); 
                // Aircraft tag
                ctx.fillText(callsign, aircraftX + 60, aircraftY - 60);
                ctx.fillText(textAlt + verticalState + textAssAlt,
                            aircraftX + 60, aircraftY - 45);
                ctx.fillText(aircraft.indicatedAirSpeed + " " + aircraft.model,
                            aircraftX + 60, aircraftY - 30);   
            }

            // for selected and highlighted aircrafts: render destination and headings circle
            if (aircraft.isSelected || this.m_highlightedAircraft === aircraft.flight.callsign) {
                // Aircraft route
                let destX = this.lonToX(aircraft.flight.destination.longitude),
                    destY = this.latToY(aircraft.flight.destination.latitude),
                    destRadius = this.nmToPx(this.m_app.destinationRadius),
                    destAltitude = (aircraft.flight.destinationAltitude < 10000? 
                                    `${aircraft.flight.destinationAltitude} ft`:
                                    `FL${aircraft.flight.destinationAltitude / 100}`);

                // Aircraft destination
                ctx.beginPath();

                if (this.emphasizeDestination)
                    ctx.fillStyle = "rgba(133, 153, 0, 0.75)";
                else
                    ctx.fillStyle = "rgba(133, 153, 0, 0.5)";

                ctx.arc(destX, destY, destRadius, 0, 2 * Math.PI);
                ctx.fill();

                ctx.fillStyle = "rgba(133, 153, 0, 1)";
                ctx.fillText(destAltitude, destX + destRadius, destY - destRadius);

                // headings circle 
                let headingsRadius = 150;
                let majorTickLength = 10;
                let minorTickLength = 5;

                ctx.beginPath();

                if (aircraft.isSelected && (this.emphasizeHeadings ||  
                    Math.sqrt(Math.pow(this.m_mouseX - aircraftX, 2) + 
                              Math.pow(this.m_mouseY - aircraftY, 2)) < headingsRadius + majorTickLength + 20)) {
                    ctx.strokeStyle = "rgba(42, 161, 152, 0.75)";
                    ctx.fillStyle = "rgba(42, 161, 152, 0.75)";
                } else {
                    ctx.strokeStyle = "rgba(42, 161, 152, 0.25)";
                    ctx.fillStyle = "rgba(42, 161, 152, 0.25)";
                }

                ctx.font = "0.8em shareTech";
                ctx.textBaseline = "middle";
                ctx.textAlign = "center";

                ctx.arc(aircraftX, aircraftY, headingsRadius, 0, 360);
    
                for (let heading = 15; heading <= 360; heading += 15) {
                    let headingRad = Number(heading).toRad();
                    let tickLength = ((heading % 45) == 0? majorTickLength: minorTickLength);

                    ctx.moveTo(aircraftX + headingsRadius * Math.sin(headingRad),
                               aircraftY + headingsRadius * Math.cos(headingRad));
                    ctx.lineTo(aircraftX + (headingsRadius + tickLength) * Math.sin(headingRad), 
                               aircraftY + (headingsRadius + tickLength) * Math.cos(headingRad));

                    ctx.fillText(`${heading}`,
                        aircraftX + (headingsRadius + tickLength + 15) * Math.sin(headingRad), 
                        aircraftY - (headingsRadius + tickLength + 15) * Math.cos(headingRad));
                }

                ctx.stroke();

                ctx.font = "1em shareTech";
                ctx.textBaseline = "alphabetic";
                ctx.textAlign = "start";

            }
        }
        
        //Airport
        for(const airport of this.m_app.atcSector.airports){
            for(const runway of airport.runways) {
                ctx.beginPath();
                ctx.strokeStyle = "#657983";
                ctx.lineWidth = 3;
                ctx.moveTo(this.lonToX(runway.thresholds[0].longitude), this.latToY(runway.thresholds[0].latitude));
                ctx.lineTo(this.lonToX(runway.thresholds[1].longitude), this.latToY(runway.thresholds[1].latitude));
                ctx.stroke();
            }

        }          
    }

    _showMouseCoordinates() {
        $("#mouseXY").text(`${this.m_mouseX}, ${this.m_mouseY}`);        
        $("#mouseLat").text(this.yToLat(this.m_mouseY));
        $("#mouseLon").text(this.xToLon(this.m_mouseX));
        $("#transformation").text(`${Math.round(this.m_translationX)},` + 
                                  `${Math.round(this.m_translationY)},` + 
                                  `${Math.round(this.m_zoomLevel * 100) / 100}`);
    }
}