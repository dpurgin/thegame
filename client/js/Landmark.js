/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import {
    Latitude, 
    Longitude
} from "./Coordinate";

export class Landmark {
    constructor(name, latitude, longitude) {
        console.assert(latitude instanceof Latitude, "latitude instanceof Latitude");
        console.assert(longitude instanceof Longitude, "longitude instanceof Longitude");

        this.m_name = String(name);
        this.m_latitude = latitude;
        this.m_longitude = longitude;
    }

    distanceTo(lat, lon = null) {
        if (lat instanceof Landmark) { 
            return this.distanceTo(lat.latitude, lat.longitude);
        }

        console.assert(lat instanceof Latitude, "lat instanceof Latitude");
        console.assert(lon instanceof Longitude, "lon instanceof Longitude");

        return Math.sqrt(Math.pow(lat.toNumeric() - this.latitude.toNumeric(), 2) + 
                         Math.pow(lon.toNumeric() - this.longitude.toNumeric(), 2));
    }

    headingTo(lat, lon = null) { 
        if (lat instanceof Landmark) {
            return this.headingTo(lat.latitude, lat.longitude);
        } 
        
        console.assert(lat instanceof Latitude, "lat instanceof Latitude");
        console.assert(lon instanceof Longitude, "lon instanceof Longitude");

        let heading = 0;

        if (this.latitude.toNumeric() < lat.toNumeric() && 
            this.longitude.toNumeric() < lon.toNumeric()) {

            let sinus = (lon.toNumeric() - this.longitude.toNumeric()) / this.distanceTo(lat, lon);
            heading = Math.asin(sinus) * 180 / Math.PI;

        } else if (this.latitude.toNumeric() < lat.toNumeric() && 
                   this.longitude.toNumeric() > lon.toNumeric()) {

            let sinus = (this.longitude.toNumeric() - lon.toNumeric()) / this.distanceTo(lat, lon);
            heading = 360 - Math.asin(sinus) * 180 / Math.PI;

        } else if (this.latitude.toNumeric() > lat.toNumeric() &&
                   this.longitude.toNumeric() < lon.toNumeric()) {
            
            let sinus = -(lat.toNumeric() - this.latitude.toNumeric()) / this.distanceTo(lat, lon);
            heading = Math.asin(sinus) * 180 / Math.PI + 90;

        } else if (this.latitude.toNumeric() > lat.toNumeric() && 
                   this.longitude.toNumeric() > lon.toNumeric()) {

            let sinus = (this.longitude.toNumeric() - lon.toNumeric()) / this.distanceTo(lat, lon);
            heading = Math.asin(sinus) * 180 / Math.PI + 180;
        }

        return (heading == 0? 360: heading);
    }

    get name() { return this.m_name; }
    get latitude() { return this.m_latitude; }
    get longitude() { return this.m_longitude; }
}

// Navigation point 
export class Fix extends Landmark {

}

export class Runway extends Landmark {
    constructor(name, latitude, longitude, length, width, bearing, thresholds) {
        super(name, latitude, longitude);

        this.m_bearing = bearing; 
        this.m_length = length; 
        this.m_width = width;
        this.m_thresholds = thresholds;

        for (let threshold of this.m_thresholds) {
            threshold._runway = this;
        }
    }

    get length() { return this.m_length; }
    get width() { return this.m_width; }
    get bearing() { return this.m_bearing; }
    get thresholds() {return this.m_thresholds; }
}

export class RunwayThreshold extends Landmark {
    constructor(name, latitude, longitude, altitude, glideslopeGradient, heading) {
        super(name, latitude, longitude);

        this.m_altitude = altitude;
        this.m_glideslopeGradient = glideslopeGradient; 
        this.m_heading = heading;
        this.m_runway = null;
    }

    get altitude() {return this.m_altitude;}
    get glideslopeGradient() {return this.m_glideslopeGradient;}
    get heading() { return this.m_heading; }
    get runway() { return this.m_runway; }

    set _runway(rwy) { this.m_runway = rwy; } 
}

export class Airport extends Landmark {
    constructor(name, latitude, longitude, altitude, runways) {
        super(name, latitude, longitude);
        
        this.m_altitude = altitude;

        this.m_landingThreshold = runways[0].thresholds[0];
        this.m_departingThreshold = runways[0].thresholds[0];

        this.m_runways = runways;
        this.m_runwaysMap = new Map();

        for (const rwy of runways) { 
            this.m_runwaysMap.set(rwy.name, rwy);
        }
    }
    
    get landingThreshold() { return this.m_landingThreshold; } 
    get departingThreshold() { return this.m_departingThreshold; }

    get runways() {return this.m_runways; }

    set landingThreshold(val) { 
        this._setActiveThreshold("m_landingThreshold", val);
    } 

    set departingThreshold(val) { 
        this._setActiveThreshold("m_departingThreshold", val);
    }

    _setActiveThreshold(prop, val) { 
        console.assert(typeof(prop) === "string", "_setActiveThreshold(): prop must be string");
        console.assert(typeof(val) === "string", "_setActiveThreshold(): val must be string");

        this[prop] = null;

        for (let i = 0; i < this.m_runways.length && this[prop] === null; ++i) {  
            for (let j = 0; j < this.m_runways[i].thresholds.lenth && this[prop] === null; ++j) {
                if (this.m_runways[i].thresholds[j].name === val) {
                    this[prop] = this.m_runways[i].thresholds[j];
                }
            }
        }

        if (this[prop] === null) {
            console.error(`Threshold not found: ${val}, setting to ${this.m_runways[0].thresholds[0].name}`);
            this[prop] = this.m_runways[0].thresholds[0];
        }
    }
}