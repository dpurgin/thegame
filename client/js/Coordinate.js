/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

export class Coordinate {
    constructor(val) { 
        if (val instanceof Coordinate) {
            this.m_value = val.toNumeric();
        } else if (!isNaN(val) && isFinite(val)) {
            this.m_value = val;
        } else if (typeof(val) === "string") {
            let re = /^(\d\d\d?) (\d\d) (\d\d\.\d\d)([NESW])$/;

            console.assert(re.test(val));

            let matches = re.exec(val);

            this.m_value = Number(matches[1]) + 
                           Number(matches[2]) / 60 + 
                           Number(matches[3]) / 3600;
            
            this.m_value *= (matches[4] == "N" || matches[4] == "E"? 1: -1);
        } else {
            console.error("Unsupported data for Coordinate: " + val);
        }
    }

    toString() {
        let val = Math.abs(this.m_value);
        let deg = Math.trunc(val);
        let min = Math.trunc((val - deg) * 60); 
        let sec = Math.trunc((val - deg - min / 60) * 3600); 
        let msec = (val - deg - min / 60 - sec / 3600) * 3600000;

        return String(deg).padLeft(2, "0") + " " + 
               String(min).padLeft(2, "0") + " " + 
               String(sec).padLeft(2, "0") + "." + String(Math.round(msec / 10)).padRight(2, "0");
    }

    toNumeric() { 
        return this.m_value; 
    }

    set(val) { 
        this.m_value = val;
    }
}

export class Longitude extends Coordinate {
    toString() { 
        return super.toString().padLeft(12, "0") + (this.toNumeric() > 0? "E": "W");
    }
}

export class Latitude extends Coordinate {
    toString() { 
        return super.toString() + (this.toNumeric() > 0? "N": "S");
    }
}