/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

export class NavigationProcedure {
    constructor(name, tts, route) {         
        console.assert(typeof(name) === "string", "typeof(name) === \"string\"");
        console.assert(typeof(tts) === "string", "typeof(tts) === \"string\"");
        console.assert(route instanceof Array, "route instanceof Array");

        this.m_name = name; 
        this.m_tts = tts;
        this.m_route = route; 
    }

    get name() { return this.m_name; } 
    get tts() { return this.m_tts; } 
    get route() { return this.m_route; } 
}

export class STAR extends NavigationProcedure { 
    constructor(name, tts, route, altitudes) { 
        super(name, tts, route); 

        console.assert(altitudes instanceof Array, "altitude instanceof Array");

        this.m_altitudes = altitudes; 
        
    }

    get altitudes() { return this.m_altitudes; } 
}

export class SID extends NavigationProcedure {
    constructor(name, tts, route, initialClimb, initialTurn, destinations, destinationAltitudes) {
        super(name, tts, route); 

        console.assert(isFinite(initialClimb) && !isNaN(initialClimb) && initialClimb > 0, 
                       "initialClimb");
        console.assert(initialTurn === "none" || initialTurn === "left" || initialTurn === "right", 
                       "initialTurn");

        this.m_initialClimb = initialClimb; 
        this.m_initialTurn = initialTurn;
        this.m_destinations = destinations;
        this.m_destinationAltitudes = destinationAltitudes;
    }

    get initialClimb() { return this.m_initialClimb; } 
    get initialTurn() { return this.m_initialTurn; } 
    get destinations() { return this.m_destinations; }
    get destinationAltitudes() { return this.m_destinationAltitudes; }
}