/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";

export default class ScoreboardModalController extends UIController {
    _initialize() {
        $("#scoreBoardModal").on("show.bs.modal", () => this._updateScoreboard());

        this.m_table = $("#scoreBoardModal table > tbody");
    }

    _updateScoreboard() {
        this.m_table.children().remove();        
        this.m_table.append(
            `<tr>
                <th>Ranking</th>
                <th>Name</thl>
                <th>Sector</th>
                <th>Acft's Served</th>
                <th>Conflict</th>
                <th>Total Time</th>
            </tr>`  
        );

        $.getJSON("/api/scores", (result) => this._fillScoreBoard(result));
    }

    _fillScoreBoard(response) {
        if (response["success"]) {
            for (let i = 1; i <= response["data"].length; ++i) {
                let item = response["data"][i - 1];

                this.m_table.append(
                    `<tr>
                        <td>${i}</td>
                        <td>${item.name}</td>
                        <td>${item.sector}</td>
                        <td>${item.served}</td>
                        <td>${this._parseElapsed(item.conflict)}</td>
                        <td>${this._parseElapsed(item.elapsed)}</td>
                     </tr>`);                
            }
        }
    }       
}