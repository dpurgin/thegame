/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";

export default class OpenSectorDialogController extends UIController {
    _initialize() {
        let me = this;

        $("#openSectorModal").on("show.bs.modal", () => this._onShow());

        $("#openSectorBrowse").change(function() { 
            me._onBrowseChange(this); 
        });    

        $("#openSectorButton").click(function() { 
            me._onOpenClick(this); 
        });        
    }

    _onBrowseChange(el) {
        let hasSelected = (el.files.length > 0);

        if (hasSelected) {
            $("#openSectorFile").val(el.files[0].name);
            $("#openSectorButton").removeProp("disabled");
        } else {
            $("#openSectorButton").prop("disabled", true);
        }
    }

    _onOpenClick(el) {
        $("#openSectorProgressContainer > .progress-bar").
            width("0%").
            removeClass("progress-bar-danger");

        $("#openSectorProgressContainer").show();

        let file = $("#openSectorBrowse")[0].files[0];

        if (file) {
            function onReaderError() {
                $("#openSectorProgressContainer > .progress-bar")
                    .width("100%")
                    .text("Error!")
                    .addClass("progress-bar-danger");

                console.log("Error loading sector file");
            }

            let fileReader = new FileReader();

            fileReader.readAsText(file);

            fileReader.onprogress = (evt) => {
                $("#openSectorProgressContainer > .progress-bar").width(
                    `${evt.loaded / evt.total * 100}%`);
            };

            fileReader.onerror = onReaderError;

            fileReader.onload = () => {
                try {
                    let sectorData = JSON.parse(fileReader.result);                
                    this.m_app.loadSector(sectorData);

                    $("#openSectorProgressContainer > .progress-bar")
                        .text("Done!");

                    $("#openSectorModal").modal("hide");

                    this.m_app.renderer.centerOn(
                        this.m_app.atcSector.centerFix.longitude, 
                        this.m_app.atcSector.centerFix.latitude, 
                        this.m_app.atcSector.centerZoomLevel);  
                } catch(e) {
                    onReaderError();
                    
                    console.log("Error parsing sector file");
                    console.log(e);
                }
            };        
        } else {
            let sector = $("#sectorFromServer").val();

            $.getJSON(`/api/sectors/${sector}`, {}, (response) => {
                if (response["success"]) {
                    try {                    
                        this.m_app.loadSector(response["data"]);

                        $("#openSectorProgressContainer > .progress-bar")
                            .text("Done!");

                        $("#openSectorModal").modal("hide");

                        this.m_app.renderer.centerOn(
                            this.m_app.atcSector.centerFix.longitude, 
                            this.m_app.atcSector.centerFix.latitude, 
                            this.m_app.atcSector.centerZoomLevel);  
                    } catch(e) {
                        $("#openSectorProgressContainer > .progress-bar")
                            .width("100%")
                            .text("Error!")
                            .addClass("progress-bar-danger");

                        console.log("Error loading sector");                        
                    }
                }
            });
        }
    }

    _onShow() {
        $("#sectorFromServer").children().remove();

        $.getJSON("/api/sectors", {}, (response) => {
            if (response["success"]) {
                for (const item of response["data"]) {
                    $("#sectorFromServer").append(`<option value="${item["file"]}">${item["title"]} - ${item["callsign"]}</option`);
                }

                if (response["data"].length > 0) {
                    $("#openSectorButton").removeProp("disabled");
                    $("#sectorFromServer").val(response["data"][0].file);
                }
            }
        });
    }
}