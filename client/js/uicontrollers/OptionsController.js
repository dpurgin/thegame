/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";

export default class OptionsController extends UIController {
    _initialize() {
        let me = this; 

        this.m_app.connect("themeChanged",
            (...args) => this._onAppThemeChanged(...args));

        this.m_app.connect("difficultyLevelChanged", 
            (...args) => this._onAppDifficultyLevelChanged(...args));

        this.m_app.connect("speedUpChanged", 
            (...args) => this._onAppSpeedUpChanged(...args));

        this.m_app.radioStack.connect("atcVoiceChanged", 
            (...args) => this._onRadioStackAtcVoiceChanged(...args));

        $("#themeLight").change(() => this.m_app.theme = "light");
        $("#themeDark").change(() => this.m_app.theme = "dark");

        $("#diffEasy").change(() => this.m_app.difficultyLevel = "easy");
        $("#diffNormal").change(() => this.m_app.difficultyLevel = "normal");
        $("#diffHard").change(() => this.m_app.difficultyLevel = "hard");
        $("#diffImpossible").change(() => this.m_app.difficultyLevel = "impossible");

        $("#speedUpNormal").change(() => this.m_app.speedUp = 1.0);        
        $("#speedUpFast").change(() => this.m_app.speedUp = 3.0);        
        $("#speedUpUltraFast").change(() => this.m_app.speedUp = 6.0);        
        
        $("#selectATCVoice").change(() => this._onSelectAtcVoiceChanged());        
        $("#testAtcVoice").click(() => me._onTestAtcVoice(this));

        $("#optionsAcceptButton").click(() => me._onOptionsAcceptButtonClicked());

        for (const voice of responsiveVoice.getVoices()) {
            $("#selectATCVoice").append(`<option value="${voice.name}">${voice.name}</option>`);
        }

        let optionsItem = localStorage.getItem("options");
        let options = this._jsonParseSafe(optionsItem);

        if (!options) {
            options = {
                "theme": "light",
                "difficultyLevel": "normal",
                "speedUp": 1,
                "atcVoice": "UK English Female"
            };
        }

        this.m_app.theme = options["theme"];
        this.m_app.difficultyLevel = options["difficultyLevel"];
        this.m_app.speedUp = options["speedUp"];
        this.m_app.radioStack.atcVoice = options["atcVoice"];        
    }

    _onAppDifficultyLevelChanged(difficultyLevel) {
        if (difficultyLevel === "easy") {
            $("#diffEasy").button("toggle");
        } else if (difficultyLevel === "normal") {
            $("#diffNormal").button("toggle");
        } else if (difficultyLevel === "hard") {
            $("#diffHard").button("toggle");
        } else if (difficultyLevel === "impossible") {
            $("#diffImpossible").button("toggle");
        }
    }

    _onAppThemeChanged(theme) { 
        if (theme === "dark") {
            $("#themeDark").button("toggle");
        } else {
            $("#themeLight").button("toggle");
        }
    }

    _onAppSpeedUpChanged(speedUp) { 
        if (speedUp === 1) {
            $("#speedUpNormal").button("toggle");
        } else if (speedUp === 3.0) {
            $("#speedUpFast").button("toggle");
        } else if (speedUp === 6.0) {
            $("#speedUpUltraFast").button("toggle");
        }
    }

    _onOptionsAcceptButtonClicked() {
        localStorage.setItem("options", JSON.stringify({
            "theme": this.m_app.theme, 
            "difficultyLevel": this.m_app.difficultyLevel, 
            "speedUp": this.m_app.speedUp,
            "atcVoice": this.m_app.radioStack.atcVoice
        }));

        $("#gameplayOptionsModal").modal("toggle");
    }

    _onRadioStackAtcVoiceChanged(voice) { 
        if (voice === "default") 
            $("#selectATCVoice")[0].selectedIndex = 0;
        else
            $("#selectATCVoice").val(voice);
    }

    _onSelectAtcVoiceChanged() { 
        this.m_app.radioStack.atcVoice = $("#selectATCVoice").val();
    }

    _onTestAtcVoice() {
        responsiveVoice.speak("All stations, radio check. Five, four, tree, two, one.", 
                              this.m_app.radioStack.atcVoice === "default"? 
                                null: 
                                this.m_app.radioStack.atcVoice);
    }

    _jsonParseSafe(data) { 
        let result = null;

        try {
            if (typeof(data) === "string") {
                result = JSON.parse(data);
            }
        } catch (e) {
            console.warn(e);
            result = null;
        }

        return result;
    }
}