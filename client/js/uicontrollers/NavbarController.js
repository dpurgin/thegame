/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";

export default class NavbarController extends UIController { 
    _initialize() {
        let me = this; 
        
        me.m_app.connect("isRunningChanged", val => me._onAppIsRunningChanged(val));        
        me.m_app.connect("sectorLoaded", val => me._onAppSectorLoaded(val));
        me.m_app.connect("statsUpdated", val => me._onAppStatsUpdated(val));

        $("#navbarStats").hide();
        $("#navbarSectorControl").hide();

        $("#navbarSectorControlStart").click(function() { 
            me._onStartClick(this);
        });

        $("#navbarSectorControlStop").click(function() { 
            me._onStopClick(this);
        });

        me._onAppIsRunningChanged(false);
    }

    _onAppIsRunningChanged(isRunning)  {
        $("#navbarSectorControlStart").prop("disabled", isRunning);
        $("#navbarSectorControlStop").prop("disabled", !isRunning);
    }

    _onAppSectorLoaded(success) { 
        if (success) {
            $("#navbarSectorControl").show(400);
            $("#navbarSectorControlTitle").text(this.m_app.atcSector.title);
        } else {
            $("#navbarSectorControl").hide(400);
        }
    }

    _onAppStatsUpdated(stats) { 
        let elapsedStr = this._parseElapsed(stats.get("elapsed"));
        let conflictStr = this._parseElapsed(stats.get("conflict"));

        // $("#navbarStats").text(
        //     `${elapsedStr}, ${stats.get("aircrafts")} aircraft(s), ${stats.get("served")} served`);

        $("#navbarStatsContainer").html(
            `<div class="stats-item">
                <span class="glyphicon glyphicon-time"></span>
                <span>${elapsedStr}</span>
             </div>
             <div class="stats-item">
                <span class="glyphicon glyphicon-alert"></span>
                <span>${conflictStr}</span>
             </div>
             <div class="stats-item">
                <span class="glyphicon glyphicon-plane"></span>
                <span>${stats.get("aircrafts")}</span>
             </div>
             <div class="stats-item">
                <span class="glyphicon glyphicon-send"></span>
                <span>${stats.get("served")}</span>
             </div>
            `
        );
    }

    _onStartClick(el) {
        this.m_app.start();

        $("#navbarStats").fadeIn(400);
    }

    _onStopClick(el) { 
        this.m_app.stop();
    }

}