/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";

export default class MainOptionsModalController extends UIController{
    _initialize(){
        $("#mainOptionsModal").modal("show");

        $("#playerNameInput").change(() => this.m_app.playerName = $("#playerNameInput").val());

        this.m_app.connect("playerNameChanged", (...args) => this._onAppPlayerNameChanged(...args));                

        let data = localStorage.getItem("playerSettings");
        let playerSettings = {};

        try {
            playerSettings = JSON.parse(data);
        } catch(e) {
            console.warn(e);

            playerSettings = null;
        }

        if (!playerSettings) {
            playerSettings = {
                "name": "Max Mustermann"
            };
        }

        this.m_app.playerName = playerSettings["name"];
    }

    _onAppPlayerNameChanged(name) {
        $("#playerNameInput").val(name);
    }
}