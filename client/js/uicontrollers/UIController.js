/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Observable from "./../Observable";

export default class UIController extends Observable {
    constructor(app) { 
        super();

        this.m_app = app;

        $(document).ready(() => this._initialize());
    }

    _initialize() {        
        // override in subclasses
    }

    _parseElapsed(elapsed) { 
        let hr = Math.trunc(elapsed / 3600);
        let min = Math.trunc((elapsed - hr * 3600) / 60);
        let sec = Math.round(elapsed - hr * 3600 - min * 60); 

        let elapsedStr = String(hr).padLeft(2, "0") + ":" + 
                         String(min).padLeft(2, "0") + ":" + 
                         String(sec).padLeft(2, "0");

        return elapsedStr;
    }       
}