/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIController from "./UIController";
import { Fix } from "./../Landmark";

export default class CommandsMenuController extends UIController {
    _initialize() { 
        let me = this;

        me.m_callsign = null; 
        me.m_altitude = 0;
        me.m_turnHeading = 0;
        me.m_turnDirection = "left";

        me.m_app.connect("sectorLoaded", (success) => me._onAppSectorLoaded(success));
        me.m_app.connect("aircraftAdded", () => me._rebuildAircraftList());
        me.m_app.connect("aircraftRemoved", () => me._rebuildAircraftList());
        me.m_app.connect("aircraftSelected", (...args) => me._onAppAircraftSelected(...args));
        me.m_app.connect("aircraftsDeselected", () => me._onAppAircraftsDeselected());

        me.m_app.radioStack.connect(
            "radioTransmitting", (...args) => me._onRadioTransmitting(...args));

        $("#selectAircraft").change(function() { me._onSelectAircraftChange($(this)); });
        $("#selectAltitude").change(function() { me._onSelectAltitudeChange($(this)); });
        $("#inputHeading").change(function() { me._onInputHeadingChange($(this)); });
        $("#selectHeadingQuadrant").click(function() { me._onSelectHeadingQuadrantClick($(this)); });
        $("#selectHeadingDirection").click(function() { me._onSelectHeadingDirectionClick($(this)); });

        $("#doCommand").click(function() { me._onDoCommandClick($(this)); });

        for (let i = 35000; i >= 3000; i -= 1000) { 
            let repr = (i >= 10000? 
                            `FL${String(i / 100).padLeft(3, "0")}—`: 
                            "") + `${i} ft`;

            $("#selectAltitude").append(`<option value="${i}">${repr}</option>`);
        }

        me._fillHeadings("I");

        $("#flightstrip").hide();
        $("#doCommand").prop("disabled", true);

        $("#headingFormGroup").on("mouseover", function() {
            me.m_app.renderer.emphasizeHeadings = true;
        });

        $("#headingFormGroup").on("mouseout", function() {
            me.m_app.renderer.emphasizeHeadings = false;
        });

        $("#commandsMenu").hide(400);
    }

    _fillHeadings(quadrant) { 
        let el = $("#selectHeading");
        let start = 0;
        let end = 0;
        let step = 10;

        if (quadrant == "I") {
            start = 0; 
            end = 90;
        } else if (quadrant == "II") {
            start = 90;
            end = 180;
        } else if (quadrant == "III") {
            start = 180; 
            end = 270; 
        } else if (quadrant == "IV") { 
            start = 270; 
            end = 360;
        }

        el.children().remove();

        for (let i = start; i <= end; i += step) { 
            el.append(`<li><a>${i == 0? 360: i}&deg;`);
        }

        let me = this;
        $("#selectHeading > li").click(function() { me._onSelectHeadingClick($(this)); });
    }

    _onAppAircraftSelected(acft) {
        $("#selectAircraft").val(acft.flight.callsign);

        this._selectAircraft(acft.flight.callsign);
    }

    _onAppAircraftsDeselected() {
        this.m_callsign = null;

        $("#selectAltitude").attr("disabled", true);
        $("#inputHeading").attr("disabled", true);
        $("#doCommand").attr("disabled", true);

        $("#flightstrip").hide(400);        
    }

    _onAppSectorLoaded(success) { 
        if (success) {
            $("#commandsMenu").show(400);
        } else {
            $("#commandsMenu").hide(400);
        }        
    }

    _onDoCommandClick() {
        let acft = this.m_app.getAircraft(this.m_callsign);

        let command = {};

        if (acft.assignedAltitude != this.m_altitude)
            command["altitude"] = this.m_altitude;
        
        if (acft.assignedHeading != this.m_turnHeading) {
            command["turnDirection"] = this.m_turnDirection;
            command["turnHeading"] = this.m_turnHeading;
        }

        this.m_app.radioStack.command(acft, command);

    }

    _onInputHeadingChange(el) { 
        this.m_turnHeading = Number(el.val()) % 360;

        if (this.m_turnHeading === 0)
            this.m_turnHeading = 360;
    }

    _onRadioTransmitting(hasTransmission) {
        if (hasTransmission) {
            $("#doCommand").addClass("btn-danger")
                           .attr("disabled", true)
                           .removeClass("btn-success");
        } else if (this.m_callsign) {
            $("#doCommand").addClass("btn-success")
                           .removeClass("btn-danger")
                           .removeAttr("disabled");
        }
    }

    _onSelectAltitudeChange(el) { 
        this.m_altitude = Number(el.val());
    }

    _onSelectAircraftChange(el) { 
        this._selectAircraft(el.val());
    }

    _onSelectHeadingDirectionClick(el) {
        this._setDirection(el.text() === "Left"? "Right": "Left");
    }

    _onSelectHeadingClick(el) {
        $("#inputHeading").val(el.text().replace(/[^\d]/, ""));
        this._onInputHeadingChange($("#inputHeading"));
    }

    _onSelectHeadingQuadrantClick(el) { 
        let quadrant = "";

        if (el.text() == "I") 
            quadrant = "II"; 
        else if (el.text() == "II") 
            quadrant = "III"; 
        else if (el.text() == "III")
            quadrant = "IV";
        else 
            quadrant = "I";

        this._setTurnQuadrant(quadrant);
    }

    _rebuildAircraftList() { 
        let selected = $("#selectAircraft").val();

        $("#selectAircraft").children().remove();

        let sortedAircrafts = [];

        this.m_app.aircrafts.every((val) => sortedAircrafts.push(val));

        sortedAircrafts.sort((a, b) => a.flight.callsign < b.flight.callsign? -1: 1);

        for (const acft of sortedAircrafts) {
            $("#selectAircraft").append(
                `<option value="${acft.flight.callsign}">
                    ${acft.flight.callsign}—${acft.flight.airline} ${acft.flight.flightNumber}
                 </option>`);
        }

        $("#selectAircraft").val(selected);
    }

    _selectAircraft(callsign) {
        this.m_callsign = callsign;

        for (const acft of this.m_app.aircrafts) {
            acft.isSelected = false;
        }

        let acft = this.m_app.getAircraft(this.m_callsign);
        acft.isSelected = true;

        this.m_altitude = acft.assignedAltitude;
        this.m_turnHeading = acft.assignedHeading;

        if (this.m_turnHeading < 90) {
            this._setTurnQuadrant("I");
        } else if (this.m_turnHeading < 180) { 
            this._setTurnQuadrant("II");
        } else if (this.m_turnHeading < 270) { 
            this._setTurnQuadrant("III");
        } else {
            this._setTurnQuadrant("IV");
        }

        $("#selectAltitude").val(this.m_altitude);
        $("#inputHeading").val(this.m_turnHeading);

        let route = "";
        let destination = "";

        for (const fix of acft.flight.route) {
            if (route.length !== 0) 
                route += " ";
            
            if (fix instanceof Fix) {
                destination = fix.name;
                route += fix.name;
            } else {
                destination = "RWY" + this.m_app.atcSector.airports[0].landingThreshold.name;
                route += destination;
            }
        }

        $("#routeLabel").text(route);
        $("#destinationLabel").text(destination);
        $("#destinationAltitudeLabel").text(acft.flight.destinationAltitude);
        
        $("#selectAltitude").removeAttr("disabled");
        $("#inputHeading").removeAttr("disabled");

        if (!this.m_app.radioStack.radioTransmitting) {
            $("#doCommand").removeAttr("disabled");
        }

        $("#flightstrip").show(400);
    }

    _setDirection(direction) {
        this.m_turnDirection = (direction === "Left"? "left": "right");

        $("#selectHeadingDirection").text(direction);
    }

    _setTurnQuadrant(quadrant) { 
        this._fillHeadings(quadrant);

        $("#selectHeadingQuadrant").text(quadrant);
    }
}