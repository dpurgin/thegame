/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import Application from "./Application";

String.prototype.padLeft = function(len, ch) {
    let  s = this;

    while (s.length < len)
        s = ch + s;

    return s;
};

String.prototype.padRight = function(len, ch) {
    let s = this;

    while (s.length < len) 
        s += ch; 

    return s;
};

Array.prototype.back = function() {
    return (this.length === 0? null: this[this.length - 1]);
};

Array.prototype.front = function() {
    return (this.length === 0? null: this[0]);
}

Array.prototype.pickOne = function() { 
    return this[Math.round(Math.random() * (this.length - 1))];
};

Number.prototype.noise = function(amount) { 
    return this + Math.random() * amount * this;
};

Number.prototype.toRad = function() {
    return this * Math.PI / 180;
};

window.g_app = new Application();