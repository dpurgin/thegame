/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import { 
    Longitude, 
    Latitude 
} from "./Coordinate";

import { Landmark } from "./Landmark";

export class Aircraft {
    constructor(flight, latitude, longitude, altitude, heading, ias, voice) {
        this.m_model = "Generic Aircraft";
        this.m_flight = flight;

        heading = Math.round(heading);

        // current parameters of the aircraft
        this.m_altitude = altitude; // math.round(m_altitude) / 100;
        this.m_heading = heading;
        this.m_indicatedAirSpeed = Math.round(ias);
        this.m_verticalSpeed = 0;
        this.m_isSelected = false;

        this.m_longitude = new Longitude(longitude);
        this.m_latitude = new Latitude(latitude);

        // target parameters set by air traffic controller
        // depending on these values the ACFT will descend/climb/turn
        this.m_assignedAltitude = altitude;
        this.m_assignedHeading = heading;
        this.m_assignedIndicatedAirSpeed = Math.round(ias);
        this.m_assignedVerticalSpeed = 0;

        // performance data. Differs for different aircraft models
        this.m_cruiseIas = 250;   // indicated airspeed for leveled flight (simplified...)
        this.m_approachIas = 220; // indicated airspeed when descending or climbing (simplified...)
        this.m_altitudeChangeRate = 1800; // feet per minute when descending or climbing
        this.m_turnRate = 3; // Standard Rate Turn, degrees per second 

        // Aircraft state - cruising, descending, climbing, turning
        // Bit 0 - cruising (set), descending or climbing (not set)
        // Bit 1 - descending (set), climbing (not set)
        // Bit 2 - turning (set), straight flight (not set)
        // Bit 3 - turning right (set), turning right (not set)
        // Bit 4 - conflict (set)
        this.m_stateBits = 0x01;

        this.m_voice = voice;
    }
    
    // Current altitude in feet 
    get altitude() { return this.m_altitude; }

    get flight() { return this.m_flight; } 

    // Speed relative to the ground in knots
    get groundSpeed() { 
        return this.altitude / 1000 + this.indicatedAirSpeed /* maybe add some wind too? */;
    }
    
    // Flying heading in degrees
    get heading() { return this.m_heading; }

    // Indicated air speed in knots
    get indicatedAirSpeed() { return this.m_indicatedAirSpeed; } 

    // Latitude in degrees
    get latitude() { return this.m_latitude; }

    // Longitude in degrees
    get longitude() { return this.m_longitude; } 

    // Aircraft model 
    get model() { return this.m_model; }

    // Vertical speed in feet per minute
    get verticalSpeed() { return this.m_verticalSpeed; }

    get voice() { return this.m_voice; }

    // retrieving aircraft state 
    get isCruising() { return ((this.m_stateBits & 0x01) == 0x01); }
    get isClimbing() { return (!this.isCruising && ((this.m_stateBits & 0x02) == 0x00)); }
    get isDescending() { return (!this.isCruising && ((this.m_stateBits & 0x02) == 0x02)); }
    get isTurning() { return ((this.m_stateBits & 0x04) == 0x04); }
    get isTurningRight() { return (this.isTurning && ((this.m_stateBits & 0x08) == 0x08)); }
    get isTurningLeft() { return (this.isTurning && ((this.m_stateBits & 0x08) == 0x00)); }
    get hasConflict() { return (this.m_stateBits & 0x10) == 0x10; }

    get assignedAltitude() { return (this.m_assignedAltitude); }
    get assignedHeading() { return this.m_assignedHeading; }

    set hasConflict(val) {
        if (val) 
            this.m_stateBits |= 0x10;
        else 
            this.m_stateBits &= ~0x10;
    }

    get isSelected() { return this.m_isSelected; }
    set isSelected(val) { this.m_isSelected = val; }

    // check if coordinates and altitude are in conflict zone of this aircraft 
    inConflictZone(latitude, longitude, altitude) {
        if (latitude instanceof Aircraft) { 
            let acft = latitude; 

            return this.inConflictZone(acft.latitude, acft.longitude, acft.altitude);
        }

        console.assert(latitude instanceof Latitude, "inConflictZone(): latitude must be Latitude");
        console.assert(longitude instanceof Longitude, "inConflictZone(): longitude must be Longitude");
        console.assert(typeof(altitude) === "number", "inConflictZone(): altitude must be number");

        let pos = new Landmark("", this.latitude, this.longitude);

        // 1 degree is 60 nm
        // horizontal separation is 5 nm
        // vertical separation is 1000 nm, consider 900 ft 

        return pos.distanceTo(latitude, longitude) * 60 < 5 && 
               Math.abs(this.altitude - altitude) < 900;
    }

    // Climb or descend 
    changeAltitude(altitude) {        
        // unset Cruising bit 
        this.m_stateBits &= ~0x01;

        // set new Assigned altitude and Assigned vertical speed
        this.m_assignedAltitude = altitude; 
        this.m_assignedIndicatedAirSpeed = this.m_approachIas;

        // set Climb or Descend bit
        if (this.m_assignedAltitude > this.m_altitude) {
            this.m_stateBits &= ~0x02; // climb 
            this.m_assignedVerticalSpeed = this.m_altitudeChangeRate;
        } else {
            this.m_stateBits |= 0x02; // descend         
            this.m_assignedVerticalSpeed = -this.m_altitudeChangeRate;
        }
    }

    // Stop climbing/descending 
    levelOff() { 
        // set cruising bit 
        this.m_stateBits |=  0x01;        

        this.m_assignedVerticalSpeed = 0; 
        this.m_assignedIndicatedAirSpeed = (this.m_altitude > 10000? this.m_cruiseIas: this.m_approachIas);
    }

    turnRight(heading) {
        console.assert(heading > 0 && heading <= 360);

        // set Turning bits
        this.m_stateBits |= 0x04;
        this.m_stateBits |= 0x08;

        this.m_assignedHeading = Math.round(heading);
    }

    turnLeft(heading) {
        console.assert(heading > 0 && heading <= 360);

        heading = Math.round(heading);

        // set Turning bits 
        this.m_stateBits |= 0x04;
        this.m_stateBits &= ~0x08;

        this.m_assignedHeading = Math.round(heading);
    }

    // Stop turning 
    flyStraight() { 
        // reset turning bit 
        this.m_stateBits &= ~0x04;
    }

    // this method should be called only from the game cycle processor 
    processGameCycle(elapsed) {
        // VS and IAS are changed instantly (for simplicity)
        this.m_verticalSpeed = this.m_assignedVerticalSpeed;
        this.m_indicatedAirSpeed = this.m_assignedIndicatedAirSpeed;

        // compute travelled distance in nautical miles for the aircraft
        // 1 kt = 1 nm per hour
        let nm = this.groundSpeed * elapsed / 3.6e6;

        // normally 1nm is 1 minute of latitude. 
        // 1 minute of longitude is not constant and should be calculated. 
        // assume, for simplicity, 1 nm = 1 min of lat and lon
        let latNmChange = nm * Math.cos(this._toRad(this.heading));
        let lonNmChange = nm * Math.sin(this._toRad(this.heading));

        this.m_latitude.set(this.m_latitude.toNumeric() + latNmChange / 60);
        this.m_longitude.set(this.m_longitude.toNumeric() + lonNmChange / 60);

        // compute altitude change
        // vertical speed is in feet per minute 
        this.m_altitude += this.verticalSpeed * elapsed / 60000;

        // level off if desired altitude is reached
        if ((this.isClimbing && this.m_altitude >= this.m_assignedAltitude) || 
            (this.isDescending && this.m_altitude <= this.m_assignedAltitude))
        {
            this.levelOff();
        }

        // compute turns 
        if (this.isTurning) {
            let headingChange = this.m_turnRate * elapsed / 1000 * (this.isTurningLeft? -1: 1);

            this.m_heading += headingChange;

            // stop turning if desired heading is reached 
            if ((this.isTurningRight && 
                    this.m_heading >= this.m_assignedHeading && this.m_heading <= this.m_assignedHeading + headingChange) ||
                (this.isTurningLeft && 
                    this.m_heading <= this.m_assignedHeading && this.m_heading >= this.m_assignedHeading + headingChange))
            {
                this.flyStraight();
            }

            // normalize heading to (0; 360]
            while (this.m_heading > 360)
                this.m_heading -= 360;

            while (this.m_heading <= 0)             
                this.m_heading += 360;        
        }
    }

    _toRad(angle) {
        return angle * (Math.PI / 180);
    }       
}

export class B737Aircraft extends Aircraft {
    constructor(...args) {
        super(...args);

        this.m_model = "B737";        

        this.m_cruiseIas = 250;
        this.m_approachIas = 220;
        this.m_altitudeChangeRate = 1800;
    }

    static create(...args) {
        return new B737Aircraft(...args);
    }
}

export class B747Aircraft extends Aircraft {
    constructor(...args) {
        super(...args);

        this.m_model = "B747";

        this.m_cruiseIas = 250;
        this.m_approachIas = 210;
        this.m_altitudeChangeRate = 2000;
    }

    static create(...args) {
        return new B747Aircraft(...args);
    }
}

export class A320Aircraft extends Aircraft {
    constructor(...args) {
        super(...args);

        this.m_model = "A320";

        this.m_cruiseIas = 250;
        this.m_approachIas = 220;
        this.m_altitudeChangeRate = 2400;
    }

    static create(...args) {
        return new A320Aircraft(...args);
    }
}

export class A330Aircraft extends Aircraft {
    constructor(...args) {
        super(...args);

        this.m_model = "A330";

        this.m_cruiseIas = 250;
        this.m_approachIas = 220;
        this.m_altitudeChangeRate = 2200;
    }

    static create(...args) {
        return new A330Aircraft(...args);
    }
}