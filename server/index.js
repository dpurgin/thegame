/*
Copyright (c) <2017> Dmitriy Purgin <dmitriy.purgin@students.fh-hagenberg.at>
                     Andreas Grigoriadis <andreas.grigoriadis@students.fh-hagenberg.at>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");

function _scoreFindCallbackGenerator(name, sector) {
    return (item) => item.name === name && item.sector === sector;
}

function _scoresComparator(a, b) {
    if (a.served > b.served || 
        (a.served == b.served && a.conflict < b.conflict) || 
        (a.served == b.served && a.conflict == b.conflict && a.elapsed < b.elapsed)) {
            
        return -1;
    } 

    return 1;
}

function _sendError(response, message) { 
    _sendJsonResponse(response, {
        "success": false, 
        "msg": String(message)
    });
}

function _sendSuccess(response, data) { 
    _sendJsonResponse(response, {
        "success": true, 
        "data": data
    });
}

function _sendJsonFile(response, fileName) {
    fs.readFile(fileName, (err, data) => {
        let scores = [];

        if (!err) {
            try {
                scores = JSON.parse(data);
            } catch(e) {
                err = String(e);
            }
        }

        if (err)
            _sendError(response, err);
        else
            _sendJsonResponse(response, {
                "success": true,
                "data": scores
            });
    });
}

function _sendJsonResponse(response, data){ 
    console.log(data);

    response.json(data);
}

function getScore(req, res) {
    try {
        let data = fs.readFileSync("private/scores.json");
        let scores = JSON.parse(data);

        console.log(`Get name=${req.params.name}, sector=${req.params.sector}`);

        let item = scores.find(_scoreFindCallbackGenerator(req.params.name, req.params.sector));

        console.log("Item: ", item);

        if (item) {
            _sendSuccess(res, item);
        } else {
            res.status(404).send();
        }
    } catch(e) {
        _sendError(String(e));
    }
}

function getScores(req, res) {     
    _sendJsonFile(res, "private/scores.json");
}

function getSector(req, res) {
    _sendJsonFile(res, `private/data/${req.params.fileName}`);
}

function getSectors(req, res) { 
    let responseData = [];

    fs.readdir("private/data", (err, files) => {
        if (err) {
            _sendError(res, err);
        } else {
            for (const file of files) { 
                let data = fs.readFileSync(`private/data/${file}`);

                try {
                    let sector = JSON.parse(data);

                    responseData.push({
                        "file": file, 
                        "title": sector["title"],
                        "callsign": sector["callsign"]
                    });                        
                } catch(e) {
                    console.warn(e);
                }
            }

            _sendSuccess(res, responseData);                
        }
    });
}

function postScore(req, res) {
    try {
        let data = fs.readFileSync("private/scores.json");
        let scores = JSON.parse(data);

        scores.push({
            "name": req.body.name, 
            "sector": req.body.sector, 
            "elapsed": Number(req.body.elapsed), 
            "conflict": Number(req.body.conflict), 
            "served": Number(req.body.served)
        });

        scores = scores.sort(_scoresComparator);

        fs.writeFileSync("private/scores.json", JSON.stringify(scores));

        res.status(201).send();
    } catch(e) {
        _sendError(res, e);
    }
}

function putScore(req, res) {
    try {
        let data = fs.readFileSync("private/scores.json");
        let scores = JSON.parse(data);
        let item = null;

        for (const score of scores) {
            if (score.name === req.params.name && score.sector === req.params.sector) {
                item = score;
                break;
            }
        }

        if (item) {
            item.elapsed += Number(req.body.elapsed); 
            item.conflict += Number(req.body.conflict);
            item.served += Number(req.body.served);

            scores = scores.sort(_scoresComparator);

            fs.writeFileSync("private/scores.json", JSON.stringify(scores));

            _sendSuccess(res);
        } else {
            res.status(404).send();
        }

    } catch(e) {
        _sendError(res, e);
    }
}

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const PORT = 80;

app.use(express.static("public"));

let router = new express.Router();

router.route("/sectors").get(getSectors);
router.route("/sectors/:fileName").get(getSector);
router.route("/scores").get(getScores).post(postScore);
router.route("/scores/:name/:sector").get(getScore).put(putScore);

app.use("/api", router);

app.listen(PORT, () => {
    console.log(`Listening at port ${PORT}...`);
});