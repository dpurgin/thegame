# WDP Projekt von Grigoriadis Andreas 1510307066, Purgin Dmitriy 15010307098

 * Projekt-Name: The Game
 * Projekt-Typ: Game
 * Gruppenprojekt: Ja
 * Externe JS/CSS:
   * Bootstrap 3.3.7
   * jQuery 3.1.1
   * ResponsiveVoice.JS
 * Geforderte Software: 
   * NodeJS with Express Web-Server
 * Gesamter Zeitaufwand: 160

The Game is about controlling arriving and departing aircrafts so that they
reach their destinations as they must. The project uses canvas extensively,
since everything that happens is drawn new as the player gives a command,
Bootstrap modals and widgets with some gameplay options, text-to-speach
engine where the player can hear the commands he gives to the aircraft crews 
and their replies, and finally responsive design for menus.			      

## Running The Game 

In NodeJS Command Line execute the following: 
 * Go to The Game directory (where package.json is)
 * Execute `npm install`
 * Go to `server` directory 
 * Execute `node index.js`
 
Open the browser and go to localhost. Turn the speakers on. 

**The Game was tested with the recent versions of Firefox and Chrome**

